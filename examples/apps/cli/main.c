/*
 *  Copyright (c) 2016, The OpenThread Authors.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the copyright holder nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */
#include <stdio.h>
#include <string.h>

#include <assert.h>
#include <openthread-core-config.h>
#include <openthread/config.h>

#include <openthread/cli.h>
#include <openthread/diag.h>
#include <openthread/tasklet.h>
#include <openthread/platform/logging.h>

#include <openthread/thread.h>
#include <openthread/link.h>
#include <openthread/udp.h>

#include "openthread-system.h"

#if OPENTHREAD_EXAMPLES_POSIX
#include <setjmp.h>
#include <unistd.h>

jmp_buf gResetJump;


void __gcov_flush();
#endif

#if OPENTHREAD_ENABLE_MULTIPLE_INSTANCES
void *otPlatCAlloc(size_t aNum, size_t aSize)
{
    return calloc(aNum, aSize);
}

void otPlatFree(void *aPtr)
{
    free(aPtr);
}
#endif

int counter = 0;

void my_udp_send(otInstance * mp_ot_instance){
    //otCliOutputFormat("Network name:   %s",(uint32_t)otThreadGetNetworkName(mp_ot_instance));
    counter++;
    const char buf[100] = "Hello from the dongle that is evil and useless but we still need to use it. :)\0";
    otMessageInfo messageInfo;
    otUdpSocket mySocket;

    memset(&messageInfo, 0, sizeof(messageInfo));

    otIp6AddressFromString("ff03::1", &messageInfo.mPeerAddr);
    messageInfo.mPeerPort = 1234;
    messageInfo.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;

    otCliOutputFormat(otThreadErrorToString(otUdpOpen(mp_ot_instance, &mySocket, NULL, NULL)));

    otMessage *test_Message = otUdpNewMessage(mp_ot_instance, NULL);

    otCliOutputFormat(otThreadErrorToString(otMessageAppend(test_Message, buf, (uint16_t)strlen(buf))));

    otError errorUdpSend = otUdpSend(&mySocket, test_Message, &messageInfo);
    
    otCliOutputFormat(otThreadErrorToString(errorUdpSend));

    otCliOutputFormat("Done\0");

    otCliOutputFormat(otThreadErrorToString(otUdpClose(&mySocket)));
    
    if(errorUdpSend != OT_ERROR_NONE && test_Message != NULL)
    {
        otMessageFree(test_Message);
    }

}

void my_thread_init(otInstance * mp_ot_instance){

    otError error;  

    //Help messages
    otCliOutputFormat ("Help: use the following commands to control the device\r\n");
    otCliOutputFormat ("-> startDevice : start a broadcast presentation\r\n");
    otCliOutputFormat ("-> deviceState : show current device state\r\n");
    otCliOutputFormat ("-> setOnOff [\"on\"/\"off\"] : turn on/off the device\r\n");
    otCliOutputFormat ("-> accumulate [int: value] : accumulate energy\r\n");
    otCliOutputFormat ("-> sendState : send the current device state to the Android Things HCU\r\n");
    otCliOutputFormat ("-> deviceHelp : show device help\r\n\r\n");


    //Connection to network
    otCliOutputFormat("Starting nRF52840 Dongle device\r\n");
    //otCliOutputFormat ("Starting nRF52840 Dongle device");
    
    error = otThreadSetNetworkName(mp_ot_instance,"BATMAN_NETWORK");
    otCliOutputFormat ("Setting Network name: %s \r\n",otThreadErrorToString(error));

    error = otLinkSetPanId(mp_ot_instance, 46569);
    otCliOutputFormat ("Setting PAN ID: %s \r\n",otThreadErrorToString(error));

    char * copy = (char*)"";
    otExtendedPanId  xpanid;
    memcpy(xpanid.m8, copy, 8);
    
    error = otThreadSetExtendedPanId(mp_ot_instance, &xpanid);
    otCliOutputFormat ("Setting ExtendedPAN ID: %s \r\n",otThreadErrorToString(error));

    error = otLinkSetChannel(mp_ot_instance, 11);
    otCliOutputFormat ("Setting Channel: %s \r\n",otThreadErrorToString(error));

    error = otIp6SetEnabled(mp_ot_instance, true);  
    otCliOutputFormat ("Starting IPv6 interface: %s \r\n",otThreadErrorToString(error));

    error = otThreadSetEnabled(mp_ot_instance, true);
    otCliOutputFormat ("Starting Thread interface: %s \r\n",otThreadErrorToString(error));

    otCliOutputFormat ("Connection phase finished\r\n");

    otCliOutputFormat ("Connected to %s\r\n",otThreadGetNetworkName(mp_ot_instance));





}

void otTaskletsSignalPending(otInstance *aInstance)
{
    (void)aInstance;
}

int main(int argc, char *argv[])
{
    otInstance *instance;

#if OPENTHREAD_EXAMPLES_POSIX
    if (setjmp(gResetJump))
    {
        alarm(0);
#if OPENTHREAD_ENABLE_COVERAGE
        __gcov_flush();
#endif
        execvp(argv[0], argv);
    }
#endif

#if OPENTHREAD_ENABLE_MULTIPLE_INSTANCES
    size_t   otInstanceBufferLength = 0;
    uint8_t *otInstanceBuffer       = NULL;
#endif

pseudo_reset:

    otSysInit(argc, argv);

#if OPENTHREAD_ENABLE_MULTIPLE_INSTANCES
    // Call to query the buffer size
    (void)otInstanceInit(NULL, &otInstanceBufferLength);

    // Call to allocate the buffer
    otInstanceBuffer = (uint8_t *)malloc(otInstanceBufferLength);
    assert(otInstanceBuffer);

    // Initialize OpenThread with the buffer
    instance = otInstanceInit(otInstanceBuffer, &otInstanceBufferLength);
#else
    instance = otInstanceInitSingle();
#endif
    assert(instance);

    otCliUartInit(instance);
    my_thread_init(instance);

#if OPENTHREAD_ENABLE_DIAG
    otDiagInit(instance);
#endif
    /*
    while(!otSysPseudoResetWasRequested()){
        if(counter < 50){
            my_udp_send(instance);
            otTaskletsProcess(instance);
            otSysProcessDrivers(instance);
        }
    }*/
    
    /*for (int i=0; i< 200000; i++){
        otTaskletsProcess(instance);
        otSysProcessDrivers(instance);
    }*/
/*
    otTaskletsProcess(instance);
    otSysProcessDrivers(instance);
    for(int i=0;i<100;i++){
        my_udp_send(instance);
        otTaskletsProcess(instance);
        otSysProcessDrivers(instance);
    }*/
    

    while (!otSysPseudoResetWasRequested())
    {
        otTaskletsProcess(instance);
        otSysProcessDrivers(instance);
    }
    
    otCliOutputFormat("Pseudo reset was requested\0");

    otInstanceFinalize(instance);
#if OPENTHREAD_ENABLE_MULTIPLE_INSTANCES
    free(otInstanceBuffer);
#endif

    goto pseudo_reset;

    return 0;
}

/*
 * Provide, if required an "otPlatLog()" function
 */
#if OPENTHREAD_CONFIG_LOG_OUTPUT == OPENTHREAD_CONFIG_LOG_OUTPUT_APP
void otPlatLog(otLogLevel aLogLevel, otLogRegion aLogRegion, const char *aFormat, ...)
{
    OT_UNUSED_VARIABLE(aLogLevel);
    OT_UNUSED_VARIABLE(aLogRegion);
    OT_UNUSED_VARIABLE(aFormat);

    va_list ap;
    va_start(ap, aFormat);
    otCliPlatLogv(aLogLevel, aLogRegion, aFormat, ap);
    va_end(ap);
}
#endif


