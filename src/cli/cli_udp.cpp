/*
 *  Copyright (c) 2017, The OpenThread Authors.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *  2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *  3. Neither the name of the copyright holder nor the
 *     names of its contributors may be used to endorse or promote products
 *     derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 *  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 *  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 *  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 *  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 *  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file
 *   This file implements a simple CLI for the CoAP service.
 */

#include "cli_udp.hpp"

#include <openthread/message.h>
#include <openthread/udp.h>

#include "cli/cli.hpp"
#include "cli/cli_server.hpp"
#include "common/encoding.hpp"

#include <openthread/thread.h>
#include <openthread/link.h>
#include <openthread/openthread-system.h>

#include <string.h>


using ot::Encoding::BigEndian::HostSwap16;



namespace ot {
namespace Cli {



const struct UdpExample::Command UdpExample::sCommands[] = {
    {"help", &UdpExample::ProcessHelp},       {"bind", &UdpExample::ProcessBind}, {"close", &UdpExample::ProcessClose},
    {"connect", &UdpExample::ProcessConnect}, {"open", &UdpExample::ProcessOpen}, {"send", &UdpExample::ProcessSend},
    {"sendState", &UdpExample::ProcessState}, {"startDevice", &UdpExample::ProcessDevice}, {"setOnOff", &UdpExample::ProcessOnOFF},
    {"accumulate", &UdpExample::ProcessPowerAccumulated}, {"deviceState", &UdpExample::ProcessShowState}, {"deviceHelp", &UdpExample::ProcessShowHelp}};

UdpExample::UdpExample(Interpreter &aInterpreter)
    : mInterpreter(aInterpreter)
{
    memset(&mSocket, 0, sizeof(mSocket));
}


otError UdpExample::ProcessHelp(int argc, char *argv[])
{
    OT_UNUSED_VARIABLE(argc);
    OT_UNUSED_VARIABLE(argv);

    for (unsigned int i = 0; i < OT_ARRAY_LENGTH(sCommands); i++)
    {
        mInterpreter.mServer->OutputFormat("%s\r\n", sCommands[i].mName);
    }

    return OT_ERROR_NONE;
}

otError UdpExample::ProcessBind(int argc, char *argv[])
{
    otError    error;
    otSockAddr sockaddr;
    long       value;

    VerifyOrExit(argc == 2, error = OT_ERROR_INVALID_ARGS);

    memset(&sockaddr, 0, sizeof(sockaddr));

    error = otIp6AddressFromString(argv[0], &sockaddr.mAddress);
    SuccessOrExit(error);

    error = Interpreter::ParseLong(argv[1], value);
    SuccessOrExit(error);

    sockaddr.mPort    = static_cast<uint16_t>(value);
    sockaddr.mScopeId = OT_NETIF_INTERFACE_ID_THREAD;

    error = otUdpBind(&mSocket, &sockaddr);

exit:
    return error;
}

otError UdpExample::ProcessConnect(int argc, char *argv[])
{
    otError    error;
    otSockAddr sockaddr;
    long       value;

    VerifyOrExit(argc == 2, error = OT_ERROR_INVALID_ARGS);

    memset(&sockaddr, 0, sizeof(sockaddr));

    error = otIp6AddressFromString(argv[0], &sockaddr.mAddress);
    SuccessOrExit(error);

    error = Interpreter::ParseLong(argv[1], value);
    SuccessOrExit(error);

    sockaddr.mPort    = static_cast<uint16_t>(value);
    sockaddr.mScopeId = OT_NETIF_INTERFACE_ID_THREAD;

    error = otUdpConnect(&mSocket, &sockaddr);

exit:
    return error;
}

otError UdpExample::ProcessClose(int argc, char *argv[])
{
    OT_UNUSED_VARIABLE(argc);
    OT_UNUSED_VARIABLE(argv);

    return otUdpClose(&mSocket);
}

otError UdpExample::ProcessOpen(int argc, char *argv[])
{
    OT_UNUSED_VARIABLE(argc);
    OT_UNUSED_VARIABLE(argv);

    return otUdpOpen(mInterpreter.mInstance, &mSocket, HandleUdpReceive, this);
}

otError UdpExample::ProcessSend(int argc, char *argv[])
{
    otError       error;
    otMessageInfo messageInfo;
    otMessage *   message = NULL;
    int           curArg  = 0;

    memset(&messageInfo, 0, sizeof(messageInfo));

    VerifyOrExit(argc == 1 || argc == 3, error = OT_ERROR_INVALID_ARGS);

    if (argc == 3)
    {
        long value;

        error = otIp6AddressFromString(argv[curArg++], &messageInfo.mPeerAddr);
        SuccessOrExit(error);

        error = Interpreter::ParseLong(argv[curArg++], value);
        SuccessOrExit(error);

        messageInfo.mPeerPort    = static_cast<uint16_t>(value);
        messageInfo.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;
    }

    message = otUdpNewMessage(mInterpreter.mInstance, NULL);
    VerifyOrExit(message != NULL, error = OT_ERROR_NO_BUFS);

    error = otMessageAppend(message, argv[curArg], static_cast<uint16_t>(strlen(argv[curArg])));
    SuccessOrExit(error);

    error = otUdpSend(&mSocket, message, &messageInfo);

exit:

    if (error != OT_ERROR_NONE && message != NULL)
    {
        otMessageFree(message);
    }

    return error;
}

otError UdpExample::Process(int argc, char *argv[])
{
    otError error = OT_ERROR_PARSE;

    if (argc < 1)
    {
        ProcessHelp(0, NULL);
        error = OT_ERROR_INVALID_ARGS;
    }
    else
    {
        for (size_t i = 0; i < OT_ARRAY_LENGTH(sCommands); i++)
        {
            if (strcmp(argv[0], sCommands[i].mName) == 0)
            {
                error = (this->*sCommands[i].mCommand)(argc - 1, argv + 1);
                break;
            }
        }
    }
    return error;
}

//DEFEAULT HANDLER

void UdpExample::HandleUdpReceive(void *aContext, otMessage *aMessage, const otMessageInfo *aMessageInfo)
{
    static_cast<UdpExample *>(aContext)->HandleUdpReceive(aMessage, aMessageInfo);
}

void UdpExample::HandleUdpReceive(otMessage *aMessage, const otMessageInfo *aMessageInfo)
{
    mInterpreter.mServer->OutputFormat("Received something\r\n");
    uint8_t buf[1500];
    int     length;

    mInterpreter.mServer->OutputFormat("%d bytes from ", otMessageGetLength(aMessage) - otMessageGetOffset(aMessage));
    mInterpreter.mServer->OutputFormat(
        "%x:%x:%x:%x:%x:%x:%x:%x %d ", HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[0]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[1]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[2]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[3]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[4]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[5]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[6]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[7]), aMessageInfo->mPeerPort);

    length      = otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, sizeof(buf) - 1);
    buf[length] = '\0';

    mInterpreter.mServer->OutputFormat("%s\r\n", buf);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//Global variables
bool broadcastPresentationReceived = false;
otUdpSocket mySocket;
otUdpSocket uniSocket;
otMessageInfo hostInformation;


//State variables
bool isOn = true;
int powerConsumption = 0;
int powerAccumulated = 1;

////////////////////////////////////////
//Method used to start device, after initial connection to the OpentThread network, the device sends a Broadcast presentation message
otError UdpExample::ProcessDevice(int argc, char *argv[])
{
    OT_UNUSED_VARIABLE(argc);
    OT_UNUSED_VARIABLE(argv);

    otCliOutputFormat("Start broadcast presentation...\r\n");

    const char buf[250] = "presentation;name:SolarPanel;nameProductor:RiTa;model:SP3000;serialNumber:3594;isOn:true;powerConsumption:0;powerAccumulated:1;resetAccumulated:resetta la quantità di energia accumulata finora\0";
    otMessageInfo messageInfo;
    otInstance *myInstance;
    myInstance = mInterpreter.mInstance;
    otSockAddr sockaddr;

    memset(&messageInfo, 0, sizeof(messageInfo));
    memset(&hostInformation, 0, sizeof(hostInformation));
    memset(&sockaddr, 0, sizeof(sockaddr));

    otIp6AddressFromString("::", &sockaddr.mAddress);
    sockaddr.mPort    = 1234;
    sockaddr.mScopeId = OT_NETIF_INTERFACE_ID_THREAD;

    otIp6AddressFromString("ff03::1", &messageInfo.mPeerAddr);
    messageInfo.mPeerPort = 1234;
    messageInfo.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;

    otCliOutputFormat("Opening broadcast socket: %s \r\n",otThreadErrorToString(otUdpOpen(myInstance, &mySocket, HandleAckMessages, this)));

    otCliOutputFormat("Binding broadcast socket: %s \r\n",otThreadErrorToString(otUdpBind(&mySocket, &sockaddr)));

    otMessage *test_Message = otUdpNewMessage(myInstance, NULL);

    otCliOutputFormat("Creation of presentation message: %s \r\n",otThreadErrorToString(otMessageAppend(test_Message, buf, (uint16_t)strlen(buf))));

    otError errorUdpSend = otUdpSend(&mySocket, test_Message, &messageInfo);
    
    otCliOutputFormat("Sending presentation message: %s \r\n",otThreadErrorToString(errorUdpSend));
    
    if(errorUdpSend != OT_ERROR_NONE && test_Message != NULL)
    {
        otMessageFree(test_Message);
    }

    return OT_ERROR_NONE;
}

//Method used to set device on/off
otError UdpExample::ProcessOnOFF(int argc, char *argv[])
{
    otCliOutputFormat("Setting new On/Off state\r\n");

    char * newOnOffState = argv[0];
    otCliOutputFormat("Prova %s \r\n",argv[0]);
    bool previousState = isOn;
    otError error = OT_ERROR_NONE;

    if(strcmp(newOnOffState, "on") == 0){
        isOn = true;
        if (!previousState){
            powerAccumulated = 0;
        }
        powerConsumption = 5;
    }else if(strcmp(newOnOffState,"off") == 0){
        isOn = false;
        powerAccumulated = 0;
        powerConsumption = 0;
    }else{
        otCliOutputFormat("Error parsing args");
        error = OT_ERROR_INVALID_ARGS;
        return error;
    }

    otCliOutputFormat("The device is now %s\r\n", newOnOffState);

    //Send update message
    return UdpExample::ProcessState(argc,argv);
}

//Method used to accumulate new energy
otError UdpExample::ProcessPowerAccumulated(int argc, char *argv[])
{
    otCliOutputFormat("New energy accumulated\r\n");

    int newPowerAccumulated = atoi(argv[0]);

    if(isOn){
        powerAccumulated+=newPowerAccumulated;

        otCliOutputFormat("The device has accumulated  %d W\r\n", powerAccumulated);

        //Send update message
        return UdpExample::ProcessState(argc,argv);
    }else{
        otCliOutputFormat("The device is off, cannot accumulate energy\r\n");
        return OT_ERROR_ABORT;
    }

}

//Method used to show state on CLI
//TODO convertire stringhe a variabili globali
otError UdpExample::ProcessShowState(int argc, char *argv[])
{
    OT_UNUSED_VARIABLE(argc);
    OT_UNUSED_VARIABLE(argv);

    otCliOutputFormat("Device nRF52840 Dongle:\r\n");
    otCliOutputFormat("Device type: SolarPanel\r\n");
    otCliOutputFormat("Productor: RiTa\r\n");
    otCliOutputFormat("Model: SP3000\r\n");
    otCliOutputFormat("Serial Number: 3594\r\n");

    const char * isOnString = (isOn == true ) ? "true" : "false";
    otCliOutputFormat("On/Off: %s \r\n",isOnString);
    otCliOutputFormat("Power consumption: %d\r\n", powerConsumption);
    otCliOutputFormat("Power accumulated: %d\r\n", powerAccumulated);

    return OT_ERROR_NONE;

}

otError UdpExample::ProcessShowHelp(int argc, char *argv[])
{

    OT_UNUSED_VARIABLE(argc);
    OT_UNUSED_VARIABLE(argv);

    //Help messages
    otCliOutputFormat ("Help: use the following commands to control the device\r\n");
    otCliOutputFormat ("-> startDevice : start a broadcast presentation\r\n");
    otCliOutputFormat ("-> deviceState : show current device state\r\n");
    otCliOutputFormat ("-> setOnOff [\"on\"/\"off\"] : turn on/off the device\r\n");
    otCliOutputFormat ("-> accumulate [int: value] : accumulate energy\r\n");
    otCliOutputFormat ("-> sendState : send the current device state to the Android Things HCU\r\n");
    otCliOutputFormat ("-> deviceHelp : show device help\r\n");

    return OT_ERROR_NONE;
}


////Method used to notify the new state of the device using unicast messages
otError UdpExample::ProcessState(int argc, char *argv[])
{
    OT_UNUSED_VARIABLE(argc);
    OT_UNUSED_VARIABLE(argv);

    const char * isOnString = (isOn == true ) ? "true" : "false";
    char temp[10];
    char buf[100];

    //Compose update string message
    strcpy(temp,"");
    strcpy(buf,"isOn:");
    strcat(buf,isOnString);
    strcat(buf,";powerConsumption:");
    sprintf(temp,"%d",powerConsumption);
    strcat(buf,temp);
    strcat(buf,";powerAccumulated:");
    sprintf(temp,"%d",powerAccumulated);
    strcat(buf,temp);
    strcat(buf,"\0");

    otCliOutputFormat("New state: %s \r\n", buf);

    otInstance *myInstance;
    myInstance = mInterpreter.mInstance;

    otMessage *test_Message = otUdpNewMessage(myInstance, NULL);

    otCliOutputFormat("Creation of update message: %s \r\n", otThreadErrorToString(otMessageAppend(test_Message, buf, (uint16_t)strlen(buf))));

    otError errorUdpSend = otUdpSend(&uniSocket, test_Message, &hostInformation);
    
    otCliOutputFormat("Sending update message: %s \r\n", otThreadErrorToString(errorUdpSend));

    
    if(errorUdpSend != OT_ERROR_NONE && test_Message != NULL)
    {
        otMessageFree(test_Message);
    }

    return OT_ERROR_NONE;
}



//Hndler used to receive first ack confirmation from Android Things

void UdpExample::HandleAckMessages(void *aContext, otMessage *aMessage, const otMessageInfo *aMessageInfo)
{
    static_cast<UdpExample *>(aContext)->HandleAckMessage(aMessage, aMessageInfo);

}

void UdpExample::HandleAckMessage(otMessage *aMessage, const otMessageInfo *aMessageInfo)
{
    mInterpreter.mServer->OutputFormat("Received a message:\r\n");
    char buf[1500];
    int     length;

    length = otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, sizeof(buf) - 1);
    buf[length] = '\0';


    mInterpreter.mServer->OutputFormat("%d bytes from ", otMessageGetLength(aMessage) - otMessageGetOffset(aMessage));
    mInterpreter.mServer->OutputFormat(
        "%x:%x:%x:%x:%x:%x:%x:%x %d ", HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[0]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[1]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[2]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[3]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[4]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[5]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[6]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[7]), aMessageInfo->mPeerPort);


    mInterpreter.mServer->OutputFormat("%s\r\n", buf);

    /*char uniAddress[50];
    uniAddress = strcat(uniAddress,messageInfo->mPeerAddr);*/

    char * sendPortString,*temp;
    bool end = false;
    int hostReceivePort,hostSendPort;
    otSockAddr sockaddr;

    int index = 0;

    //Split message to retrieve host sending and receiving port
    while(!end){
        if(buf[index] == ':'){
            end = true;
            buf[index] ='\0';
        }
        index++;
    }

    temp = &buf[index];

    index = 0;
    end = false;

    //Split input and output port
    while(!end){
        if(temp[index] == ':'){
            end = true;
            temp[index] ='\0';
        }
        index++;
    }

    sendPortString = &temp[index];


    if(strcmp(buf,"ACK_DEVICE_CREATED") == 0){

        hostReceivePort = atoi(temp);
        hostSendPort = atoi(sendPortString);

        mInterpreter.mServer->OutputFormat("Received ACK: Host receiving port [%d] -  Host sending port [%d] \r\n",hostReceivePort,hostSendPort);

        memset(&sockaddr, 0, sizeof(sockaddr));

        hostInformation.mPeerAddr = aMessageInfo->mPeerAddr;
        hostInformation.mPeerPort = hostReceivePort;
        hostInformation.mInterfaceId = OT_NETIF_INTERFACE_ID_THREAD;

        //NON MODIFICARE IL SOCKET ADDRESS: E' L'INDIRIZZO DI PRESENTAZIONE DEL DEVICE
        //sockaddr.mAddress = aMessageInfo->mPeerAddr; 
        sockaddr.mPort    = hostSendPort;
        sockaddr.mScopeId = OT_NETIF_INTERFACE_ID_THREAD;

        otCliOutputFormat("Opening unicast receiving socket: %s \r\n",otThreadErrorToString(otUdpOpen(mInterpreter.mInstance, &uniSocket, HandleCommandMessages, this)));

        otCliOutputFormat("Binding unicast receiving socket: %s \r\n",otThreadErrorToString(otUdpBind(&uniSocket, &sockaddr)));
    }
   
}

//Handler used to receive unicast messages from Android Things : Commnands!

void UdpExample::HandleCommandMessages(void *aContext, otMessage *aMessage, const otMessageInfo *aMessageInfo)
{
    static_cast<UdpExample *>(aContext)->HandleCommandMessage(aMessage, aMessageInfo);
}

void UdpExample::HandleCommandMessage(otMessage *aMessage, const otMessageInfo *aMessageInfo)
{
    mInterpreter.mServer->OutputFormat("Received a messages:\r\n");
    char buf[1500];
    int     length;
    int index = 0;
    bool end = false;
    char * command, * parameter;
    char * argv[3];

    length      = otMessageRead(aMessage, otMessageGetOffset(aMessage), buf, sizeof(buf) - 1);
    buf[length] = '\0';


    //print message received
    mInterpreter.mServer->OutputFormat("%d bytes from ", otMessageGetLength(aMessage) - otMessageGetOffset(aMessage));
    mInterpreter.mServer->OutputFormat(
        "%x:%x:%x:%x:%x:%x:%x:%x %d ", HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[0]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[1]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[2]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[3]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[4]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[5]), HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[6]),
        HostSwap16(aMessageInfo->mPeerAddr.mFields.m16[7]), aMessageInfo->mPeerPort);

    

    mInterpreter.mServer->OutputFormat("%s\r\n", buf);


    //Split message to retrieve command and parameters
    while(!end){
        if(buf[index] == ':'){
            end = true;
            buf[index] ='\0';
        }
        index++;
    }

    command = &buf[index];
    end = false;
    index = 0;

    //Check what message has been received
    if(strcmp("COMMAND",buf) == 0){
        if(strcmp("resetAccumulated",command) == 0){
            otCliOutputFormat("Received command: resetAccumulated\r\n");
            powerAccumulated = 0;
            UdpExample::ProcessState(0, argv);
        }else{
            while(!end){
                if(command[index] == ';'){
                    end = true;
                    command[index] ='\0';
                }
                index++;
            }
            if(strcmp("onOff",command) == 0){
                parameter = &command[index];
                end = false;
                while(!end){
                    if(parameter[index] == ':'){
                        end = true;
                        parameter[index] ='\0';
                    }
                    index++;
                }

                char isOnString[10];
                char * temp = &parameter[index];

                if(strcmp(temp,"true") == 0 ){
                    strcpy(isOnString,"on");
                }else{
                    strcpy(isOnString,"off");
                }

                argv[0] = isOnString;
                //strcpy(argv[0],isOnString);
                //otCliOutputFormat("temp:  %s \r\n",temp);
                otCliOutputFormat("Received command: onOff %s \r\n",argv[0]);

                UdpExample::ProcessOnOFF(0,argv);    
            }else{
                otCliOutputFormat("Received unknown command, ignoring");
            }
            
        }
    }else{
        otCliOutputFormat("Received unknown message, ignoring");
    }
}

} // namespace Cli
} // namespace ot
