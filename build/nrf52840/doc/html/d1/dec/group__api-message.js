var group__api_message =
[
    [ "otMessage", "da/db2/structotMessage.html", [
      [ "mNext", "da/db2/structotMessage.html#a7ff8282fc7356d159ad1f7e85ccf2a37", null ]
    ] ],
    [ "otBufferInfo", "d3/de8/structotBufferInfo.html", [
      [ "m6loReassemblyBuffers", "d3/de8/structotBufferInfo.html#a0704a66abf106890138ca8aa14d3b9b9", null ],
      [ "m6loReassemblyMessages", "d3/de8/structotBufferInfo.html#aff5cc320219ff67e9aeaa024641b4810", null ],
      [ "m6loSendBuffers", "d3/de8/structotBufferInfo.html#a58d7abdc9dcf1cecf64279c3fe10ccc1", null ],
      [ "m6loSendMessages", "d3/de8/structotBufferInfo.html#a9e167353386b00a0445d137c77d22ee8", null ],
      [ "mApplicationCoapBuffers", "d3/de8/structotBufferInfo.html#adaedd8f86a2a7b08eb14dc83952fea74", null ],
      [ "mApplicationCoapMessages", "d3/de8/structotBufferInfo.html#aa0b47a58420a02a9c412099fb2b8b92c", null ],
      [ "mArpBuffers", "d3/de8/structotBufferInfo.html#ae0ef4b8579772d9b2a81586e1d0c8ff6", null ],
      [ "mArpMessages", "d3/de8/structotBufferInfo.html#a99d3ea824dde19d4aa20033fd4d62c50", null ],
      [ "mCoapBuffers", "d3/de8/structotBufferInfo.html#a295c807dd8c3683c554e8e288499f36d", null ],
      [ "mCoapMessages", "d3/de8/structotBufferInfo.html#a09770aa4559850d8dc7ab52b56d986a1", null ],
      [ "mCoapSecureBuffers", "d3/de8/structotBufferInfo.html#ad5ea195c3c8c0cfb2a6471170ff9f328", null ],
      [ "mCoapSecureMessages", "d3/de8/structotBufferInfo.html#a6f2e72caf3b382da654a6710d3896540", null ],
      [ "mFreeBuffers", "d3/de8/structotBufferInfo.html#ad857a0fb751203533d3b0df7506cab95", null ],
      [ "mIp6Buffers", "d3/de8/structotBufferInfo.html#a8d8274e7742278444335492a87d57071", null ],
      [ "mIp6Messages", "d3/de8/structotBufferInfo.html#a6824b9ad033d6b993506bfe9935a01eb", null ],
      [ "mMleBuffers", "d3/de8/structotBufferInfo.html#a39d616efd018598f24f2bf9d49c17678", null ],
      [ "mMleMessages", "d3/de8/structotBufferInfo.html#a30a9637baebd8e9ca116a74e9660abf4", null ],
      [ "mMplBuffers", "d3/de8/structotBufferInfo.html#a933f8de4b9276e3d9c2898a30b595aaf", null ],
      [ "mMplMessages", "d3/de8/structotBufferInfo.html#acef7c442842093a65956483791c701e7", null ],
      [ "mTotalBuffers", "d3/de8/structotBufferInfo.html#ab0f1372097ffb426d70ff031f7714080", null ]
    ] ],
    [ "otMessageSettings", "dc/d93/structotMessageSettings.html", [
      [ "mLinkSecurityEnabled", "dc/d93/structotMessageSettings.html#aab9429e8b3c7ab95aba741e2090afb6a", null ],
      [ "mPriority", "dc/d93/structotMessageSettings.html#ac3fe9f6ba2f79b0aac165dffd6c32229", null ]
    ] ],
    [ "otMessageQueue", "db/dd8/structotMessageQueue.html", [
      [ "mData", "db/dd8/structotMessageQueue.html#a8468309c28963ad37855a30d8073dc96", null ]
    ] ],
    [ "otBufferInfo", "d1/dec/group__api-message.html#gaadcdabf89dd602ab60efa78fad4f011f", null ],
    [ "otMessage", "d1/dec/group__api-message.html#ga62bdbaf1d492b0c5db59bd8758e38f24", null ],
    [ "otMessagePriority", "d1/dec/group__api-message.html#gaeea6ac34b255ce40ede10862e7178a70", null ],
    [ "otMessageSettings", "d1/dec/group__api-message.html#gaf37e48e3a23d9c9349abd2676a343e42", null ],
    [ "otMessagePriority", "d1/dec/group__api-message.html#gafb260c302eeeeb1a29dce59f7c86d18b", [
      [ "OT_MESSAGE_PRIORITY_LOW", "d1/dec/group__api-message.html#ggafb260c302eeeeb1a29dce59f7c86d18ba3b16d79c57f28c2f525d790897c26631", null ],
      [ "OT_MESSAGE_PRIORITY_NORMAL", "d1/dec/group__api-message.html#ggafb260c302eeeeb1a29dce59f7c86d18ba488f5dc3b298571ef13f603ebfbca25e", null ],
      [ "OT_MESSAGE_PRIORITY_HIGH", "d1/dec/group__api-message.html#ggafb260c302eeeeb1a29dce59f7c86d18ba64a2c55a23103e3249a3d049156eaf31", null ]
    ] ],
    [ "otMessageAppend", "d1/dec/group__api-message.html#gab1cef88f8ddbe6b3e274aaf6bad46c9b", null ],
    [ "otMessageFree", "d1/dec/group__api-message.html#ga70291a9d11f2f2a0bff290bf8ff4ea20", null ],
    [ "otMessageGetBufferInfo", "d1/dec/group__api-message.html#ga5eb9165303d0e72fd9cf9167784c5ab0", null ],
    [ "otMessageGetLength", "d1/dec/group__api-message.html#ga3ae928dc8b31e8808ba1b07abf3dd34b", null ],
    [ "otMessageGetOffset", "d1/dec/group__api-message.html#gafe5a5df3035db36438525eca45d9756d", null ],
    [ "otMessageGetRss", "d1/dec/group__api-message.html#ga0a3269120475ee876e2e655219dea16f", null ],
    [ "otMessageIsLinkSecurityEnabled", "d1/dec/group__api-message.html#ga6d253c0e4d97684e7e31a46abb6c25bc", null ],
    [ "otMessageQueueDequeue", "d1/dec/group__api-message.html#ga37e22ed2f43145d9d537653d5fa3cc2a", null ],
    [ "otMessageQueueEnqueue", "d1/dec/group__api-message.html#ga4d6fe74a30a119aa4ec8f484ed5cccde", null ],
    [ "otMessageQueueEnqueueAtHead", "d1/dec/group__api-message.html#gad367b244988290206e51ab3dd58628af", null ],
    [ "otMessageQueueGetHead", "d1/dec/group__api-message.html#ga71b38c9c904ecead877e9f344e213513", null ],
    [ "otMessageQueueGetNext", "d1/dec/group__api-message.html#gaefee0f07ee9fa021081afb52f74a0776", null ],
    [ "otMessageQueueInit", "d1/dec/group__api-message.html#gab51851baffa16c7f0ba802c8badd129e", null ],
    [ "otMessageRead", "d1/dec/group__api-message.html#ga7d9ded712b0d2bd70e020849a785f1d0", null ],
    [ "otMessageSetDirectTransmission", "d1/dec/group__api-message.html#ga73c39b38aaf0cff5f0a9687576af0ede", null ],
    [ "otMessageSetLength", "d1/dec/group__api-message.html#ga9aeff1951aa15aef4d5ded02ee85bb6b", null ],
    [ "otMessageSetOffset", "d1/dec/group__api-message.html#ga40a0748864a7f7efd9ef7afee724eef6", null ],
    [ "otMessageWrite", "d1/dec/group__api-message.html#gae4d611947a3d3831b5a7a803fa9bc1ef", null ]
];