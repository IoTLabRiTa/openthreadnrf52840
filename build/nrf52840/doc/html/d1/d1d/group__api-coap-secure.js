var group__api_coap_secure =
[
    [ "OT_DEFAULT_COAP_SECURE_PORT", "d1/d1d/group__api-coap-secure.html#ga3a4fe10772adba8d71cfc464f1d17740", null ],
    [ "otHandleCoapSecureClientConnect", "d1/d1d/group__api-coap-secure.html#ga140c00debcebaa6d5ce9eca413e1d320", null ],
    [ "otCoapSecureAddResource", "d1/d1d/group__api-coap-secure.html#ga14d41d91c2ce0579ddbfec429f5ec072", null ],
    [ "otCoapSecureConnect", "d1/d1d/group__api-coap-secure.html#ga9319d2fbfd22822e6c41f7aea348fa35", null ],
    [ "otCoapSecureDisconnect", "d1/d1d/group__api-coap-secure.html#ga30b21bf0b254f129b8ed52c8dbe59884", null ],
    [ "otCoapSecureGetPeerCertificateBase64", "d1/d1d/group__api-coap-secure.html#ga6a41cdb86832212736f0426ee113b236", null ],
    [ "otCoapSecureIsConnected", "d1/d1d/group__api-coap-secure.html#ga141c2451d0897088153d8bd1bd986367", null ],
    [ "otCoapSecureIsConnectionActive", "d1/d1d/group__api-coap-secure.html#ga12ee809d7363028f5bdf6e27811a0e79", null ],
    [ "otCoapSecureRemoveResource", "d1/d1d/group__api-coap-secure.html#gac692987d889f6ae39ae5b241b99a6715", null ],
    [ "otCoapSecureSendRequest", "d1/d1d/group__api-coap-secure.html#gab1e3d5b9c42ecdeab2626c8504265191", null ],
    [ "otCoapSecureSendResponse", "d1/d1d/group__api-coap-secure.html#gab5bef9705faaa54c8ead7703cf222a13", null ],
    [ "otCoapSecureSetCaCertificateChain", "d1/d1d/group__api-coap-secure.html#gaec4c8e2712955b4e24d028f291f8b96a", null ],
    [ "otCoapSecureSetCertificate", "d1/d1d/group__api-coap-secure.html#ga8acac56eeb60ed98e7ba127a86fb6faa", null ],
    [ "otCoapSecureSetClientConnectedCallback", "d1/d1d/group__api-coap-secure.html#ga3d37320684c590099db80922ea3e05da", null ],
    [ "otCoapSecureSetDefaultHandler", "d1/d1d/group__api-coap-secure.html#gaeaabcf3f2b0ea58998204fd112999d05", null ],
    [ "otCoapSecureSetPsk", "d1/d1d/group__api-coap-secure.html#ga886c10a28d5795bdcf58e9a457478742", null ],
    [ "otCoapSecureSetSslAuthMode", "d1/d1d/group__api-coap-secure.html#ga28d536c5a549d18569c3a3e2a2c6b15d", null ],
    [ "otCoapSecureStart", "d1/d1d/group__api-coap-secure.html#ga8b94749d67c30d72f486331164c4d4e0", null ],
    [ "otCoapSecureStop", "d1/d1d/group__api-coap-secure.html#gaa14d2e4e4fcca7aa1b12b3b04bf0df93", null ]
];