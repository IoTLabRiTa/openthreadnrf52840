var group__radio_config =
[
    [ "otPlatRadioGetCaps", "d1/d69/group__radio-config.html#ga5a5999dadb6ac0ee70a69a960930f132", null ],
    [ "otPlatRadioGetIeeeEui64", "d1/d69/group__radio-config.html#ga6940d730c862fae890f3a2912000002d", null ],
    [ "otPlatRadioGetPromiscuous", "d1/d69/group__radio-config.html#gaef6191a2f5c0dd177dc009dae3796e8e", null ],
    [ "otPlatRadioGetReceiveSensitivity", "d1/d69/group__radio-config.html#ga71b5cc59aaead9fc2e42b4907392a664", null ],
    [ "otPlatRadioGetTransmitPower", "d1/d69/group__radio-config.html#gadba7cca4c1176c4efeb789b1f90a5057", null ],
    [ "otPlatRadioGetVersionString", "d1/d69/group__radio-config.html#gaa8e579d7776a38b169cc4ad097e2da65", null ],
    [ "otPlatRadioSetExtendedAddress", "d1/d69/group__radio-config.html#ga62cdcfe3e94da49f13f358942a794955", null ],
    [ "otPlatRadioSetPanId", "d1/d69/group__radio-config.html#ga032e75b6d174c7cb58e8c280fdcd5f26", null ],
    [ "otPlatRadioSetPromiscuous", "d1/d69/group__radio-config.html#ga0a905ad5c76c5931ee2ef395342f0dca", null ],
    [ "otPlatRadioSetShortAddress", "d1/d69/group__radio-config.html#ga5cec7c711f3f1e21e7ce83e8019a3332", null ],
    [ "otPlatRadioSetTransmitPower", "d1/d69/group__radio-config.html#gab310e61a0ab319236d71acb02804ba02", null ]
];