var structotRadioFrame =
[
    [ "mAesKey", "dc/d04/structotRadioFrame.html#ad2691a91c1897ff33efb15836c9659d8", null ],
    [ "mChannel", "dc/d04/structotRadioFrame.html#aaca1d5593ef0b1b6dd940f6a4251dff9", null ],
    [ "mCsmaCaEnabled", "dc/d04/structotRadioFrame.html#af60d0c886464ef6b01e449a34c6a937d", null ],
    [ "mIeInfo", "dc/d04/structotRadioFrame.html#ad6994d824d77c981e4c492f2aeff4fd4", null ],
    [ "mInfo", "dc/d04/structotRadioFrame.html#a6923a03cbab0e86fe7ea56ce1fbfbbb6", null ],
    [ "mIsARetx", "dc/d04/structotRadioFrame.html#ac1d787f53a2e1fd85082492e0dc7d129", null ],
    [ "mLength", "dc/d04/structotRadioFrame.html#ac0bce2a423dd62721ac93390a151de89", null ],
    [ "mLqi", "dc/d04/structotRadioFrame.html#ae89beb2d91bf3c23e274f48430ebc4db", null ],
    [ "mMaxCsmaBackoffs", "dc/d04/structotRadioFrame.html#ad58843a28f1e1a6690827548ced85ed2", null ],
    [ "mMaxFrameRetries", "dc/d04/structotRadioFrame.html#ac663ec320ab9fe4c076e43e7d4c24c68", null ],
    [ "mMsec", "dc/d04/structotRadioFrame.html#a5afd34bc891abe58e45a8c29f9753784", null ],
    [ "mPsdu", "dc/d04/structotRadioFrame.html#a6fc89e98cc7d4dab04eabdbda239a3de", null ],
    [ "mRssi", "dc/d04/structotRadioFrame.html#a5bb7aa8af3467a0c2d845417e42c33e1", null ],
    [ "mRxInfo", "dc/d04/structotRadioFrame.html#a0a85426ffa07aa2b8c5fc9d5a774c8b2", null ],
    [ "mTxInfo", "dc/d04/structotRadioFrame.html#a2a76e70b87f07cbd6fb88f06c2d2c2e6", null ],
    [ "mUsec", "dc/d04/structotRadioFrame.html#aeec8ecd8d6837f5bcba53f21bc69c9f0", null ]
];