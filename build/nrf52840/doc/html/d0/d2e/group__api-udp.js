var group__api_udp =
[
    [ "otUdpReceiver", "de/d93/structotUdpReceiver.html", [
      [ "mContext", "de/d93/structotUdpReceiver.html#a4c832c25238195d34b824b2bfb974df0", null ],
      [ "mHandler", "de/d93/structotUdpReceiver.html#a5eba93b00ae870bf6ee7748d54508beb", null ],
      [ "mNext", "de/d93/structotUdpReceiver.html#aba84c4c8334edab40f7ae240fe0c9d70", null ]
    ] ],
    [ "otUdpSocket", "da/dfd/structotUdpSocket.html", [
      [ "mContext", "da/dfd/structotUdpSocket.html#a1988824d212b26a9b3f9cb5d412121a5", null ],
      [ "mHandle", "da/dfd/structotUdpSocket.html#ac0179b3bc55f4e14f5a5a5c7af22076e", null ],
      [ "mHandler", "da/dfd/structotUdpSocket.html#aa2ecdf82a3d6a9b21e301314de2271f0", null ],
      [ "mNext", "da/dfd/structotUdpSocket.html#af59a47912e318de493cb93710638e25d", null ],
      [ "mPeerName", "da/dfd/structotUdpSocket.html#aa853c7bac2584475eeee492aaad610ca", null ],
      [ "mSockName", "da/dfd/structotUdpSocket.html#a4a99b2565fbe000fc93add20178374bd", null ]
    ] ],
    [ "otUdpHandler", "d0/d2e/group__api-udp.html#ga281fb47e6a0668b31e5c3b173490af49", null ],
    [ "otUdpReceive", "d0/d2e/group__api-udp.html#gad6f6e0d606a8510cd082c6c57f42f4a1", null ],
    [ "otUdpReceiver", "d0/d2e/group__api-udp.html#gae0e5ce179221849c8109e1ec2c79b344", null ],
    [ "otUdpSocket", "d0/d2e/group__api-udp.html#ga2d245018891da475518da5c375beeba7", null ],
    [ "otUdpAddReceiver", "d0/d2e/group__api-udp.html#ga8898d1d888eabfdb24b0cb62106dcce9", null ],
    [ "otUdpBind", "d0/d2e/group__api-udp.html#ga646ef7ace2aa605c313d0219b8a53707", null ],
    [ "otUdpClose", "d0/d2e/group__api-udp.html#ga4654849effea287a8ed52a1b126da744", null ],
    [ "otUdpConnect", "d0/d2e/group__api-udp.html#ga057e6e28adb0cbe109675b5b781f4a36", null ],
    [ "otUdpNewMessage", "d0/d2e/group__api-udp.html#gab58e1df115551727f959616da8e6a4ca", null ],
    [ "otUdpOpen", "d0/d2e/group__api-udp.html#gac3df0bb2b8e2feeb44094d2702423cc2", null ],
    [ "otUdpRemoveReceiver", "d0/d2e/group__api-udp.html#gab65be3590610a5516beae28adf98a053", null ],
    [ "otUdpSend", "d0/d2e/group__api-udp.html#ga625eef782b209010e8a49a0d364eb10f", null ]
];