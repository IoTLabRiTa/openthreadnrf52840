var structotThreadParentResponseInfo =
[
    [ "mExtAddr", "d6/dbe/structotThreadParentResponseInfo.html#ac4df508979c986beaf6aaeeed5213026", null ],
    [ "mIsAttached", "d6/dbe/structotThreadParentResponseInfo.html#a049268445667abb4a3a738b359690724", null ],
    [ "mLinkQuality1", "d6/dbe/structotThreadParentResponseInfo.html#a3e1b335564996d8ae5e567f5b19580a4", null ],
    [ "mLinkQuality2", "d6/dbe/structotThreadParentResponseInfo.html#afdaf195f20d5e00bba34e2fe93875225", null ],
    [ "mLinkQuality3", "d6/dbe/structotThreadParentResponseInfo.html#ad83db8040ef2eebe7a10f987980e9b1b", null ],
    [ "mPriority", "d6/dbe/structotThreadParentResponseInfo.html#a02a24f96c215de2905ae4c99961728c7", null ],
    [ "mRloc16", "d6/dbe/structotThreadParentResponseInfo.html#a7337aa44092fc8e3fde2692e0d449180", null ],
    [ "mRssi", "d6/dbe/structotThreadParentResponseInfo.html#a5c6f144c86a10e7ad39d2146283c1782", null ]
];