var netdata_8h =
[
    [ "OT_NETWORK_DATA_ITERATOR_INIT", "da/d24/group__api-thread-general.html#ga50b70c8de241d3444d91337ed7e0bac5", null ],
    [ "otBorderRouterConfig", "da/d24/group__api-thread-general.html#ga021164642188962b5372ef7e49940581", null ],
    [ "otExternalRouteConfig", "da/d24/group__api-thread-general.html#ga65d4d5b42c866bce653a878f189bf1b7", null ],
    [ "otNetworkDataIterator", "da/d24/group__api-thread-general.html#gac4be34b26a43e67d12ee7e0352ee4927", null ],
    [ "otRoutePreference", "da/d24/group__api-thread-general.html#gadd1b32507e676f1e8c9c9f6ff47a6a58", null ],
    [ "otRoutePreference", "da/d24/group__api-thread-general.html#ga6085e64b90c7645b8d206edad0b14e1c", [
      [ "OT_ROUTE_PREFERENCE_LOW", "da/d24/group__api-thread-general.html#gga6085e64b90c7645b8d206edad0b14e1cafe78210da474e99dbc0c67727704f5b8", null ],
      [ "OT_ROUTE_PREFERENCE_MED", "da/d24/group__api-thread-general.html#gga6085e64b90c7645b8d206edad0b14e1ca6403ae4fe44464096ac301e6d8363523", null ],
      [ "OT_ROUTE_PREFERENCE_HIGH", "da/d24/group__api-thread-general.html#gga6085e64b90c7645b8d206edad0b14e1cab01b0823f54bf7cd11201335702ce393", null ]
    ] ],
    [ "otNetDataGet", "da/d24/group__api-thread-general.html#gad8ab00a5561346c476a9ae4ccba4ff67", null ],
    [ "otNetDataGetNextOnMeshPrefix", "da/d24/group__api-thread-general.html#ga356cdf532feba81247aaa2ee954d4ee9", null ],
    [ "otNetDataGetNextRoute", "da/d24/group__api-thread-general.html#gae302dc8a6ed2e60f7e2782f56219ec57", null ],
    [ "otNetDataGetStableVersion", "da/d24/group__api-thread-general.html#gab19461fbcb3d05db38c15c9422c7620c", null ],
    [ "otNetDataGetVersion", "da/d24/group__api-thread-general.html#gaed21de692605f5f00dbdc20c83f88b3a", null ]
];