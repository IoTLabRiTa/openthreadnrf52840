var group__api_server =
[
    [ "otServerConfig", "d9/dee/structotServerConfig.html", [
      [ "mRloc16", "d9/dee/structotServerConfig.html#a3933ef221a6f9846b274b93a377318cb", null ],
      [ "mServerData", "d9/dee/structotServerConfig.html#ab97bac0176df0c4effeaeb4ff61654f5", null ],
      [ "mServerDataLength", "d9/dee/structotServerConfig.html#a38f2600fd0478cb2aad6660695c10f81", null ],
      [ "mStable", "d9/dee/structotServerConfig.html#a6d3e2dca5c136e651e8c7404e3fb9703", null ]
    ] ],
    [ "otServiceConfig", "d5/d2a/structotServiceConfig.html", [
      [ "mEnterpriseNumber", "d5/d2a/structotServiceConfig.html#a9b0829ebb4b65f06840aadf704735387", null ],
      [ "mServerConfig", "d5/d2a/structotServiceConfig.html#a6d65ef7769a4022d9080bf2abddea9ca", null ],
      [ "mServiceData", "d5/d2a/structotServiceConfig.html#a35b1f80afa270470052a137afb4d2e06", null ],
      [ "mServiceDataLength", "d5/d2a/structotServiceConfig.html#a864a568fa1d83151c3dac7ba5b2f418f", null ],
      [ "mServiceID", "d5/d2a/structotServiceConfig.html#a590d7268b80f86aef93161c65f89f007", null ]
    ] ],
    [ "OT_SERVER_DATA_MAX_SIZE", "d2/ded/group__api-server.html#ga1fcecd055d6f3328de017a1d13aadbad", null ],
    [ "OT_SERVICE_DATA_MAX_SIZE", "d2/ded/group__api-server.html#gafad636b7480da1153404b0ed6856eab4", null ],
    [ "otServerConfig", "d2/ded/group__api-server.html#gab7eb37840d7b34b7ab13b9ebe55cc3ce", null ],
    [ "otServiceConfig", "d2/ded/group__api-server.html#ga1269ce0cec4da001d1622f29ef422b68", null ],
    [ "otServerAddService", "d2/ded/group__api-server.html#ga5f0e5f23cc7a414eff74aac7ad4d8698", null ],
    [ "otServerGetNetDataLocal", "d2/ded/group__api-server.html#ga285055c0a1dcb0550d5d5a64907f7d54", null ],
    [ "otServerGetNextLeaderService", "d2/ded/group__api-server.html#ga3c70f2e0210c33adce17f6ba4ae84568", null ],
    [ "otServerGetNextService", "d2/ded/group__api-server.html#ga8ddc6a820bb5b0b82ab4eca263b5ce5a", null ],
    [ "otServerRegister", "d2/ded/group__api-server.html#ga38e477702ffceeb3e54ea692e5bbab74", null ],
    [ "otServerRemoveService", "d2/ded/group__api-server.html#gad09c8cca1d1bbbfba498de9c0cf3cf01", null ]
];