var group__api_dns =
[
    [ "otDnsQuery", "d6/dfc/structotDnsQuery.html", [
      [ "mHostname", "d6/dfc/structotDnsQuery.html#a88531adc14c6d5ac543b1f5e2b7e6192", null ],
      [ "mMessageInfo", "d6/dfc/structotDnsQuery.html#af8337097f523924ce72bb9721810d5e7", null ],
      [ "mNoRecursion", "d6/dfc/structotDnsQuery.html#a8bc6cb5c4b4edcb4accdcca10985b142", null ]
    ] ],
    [ "OT_DNS_DEFAULT_SERVER_IP", "d2/d07/group__api-dns.html#gad6aa1d5f28809206a62df0d47b55760f", null ],
    [ "OT_DNS_DEFAULT_SERVER_PORT", "d2/d07/group__api-dns.html#gac38eb3d36e4137ecfc80410a4d923b7b", null ],
    [ "OT_DNS_MAX_HOSTNAME_LENGTH", "d2/d07/group__api-dns.html#gae58e7900acf2b5e0ad8623df42e0b134", null ],
    [ "otDnsQuery", "d2/d07/group__api-dns.html#ga6d4a109ffa3fade54010da911884e83c", null ],
    [ "otDnsResponseHandler", "d2/d07/group__api-dns.html#ga8a54778e45acfad1ae9100aaa5be2549", null ],
    [ "otDnsClientQuery", "d2/d07/group__api-dns.html#gaddb27b300fea8f2ca1648d5d181624d9", null ]
];