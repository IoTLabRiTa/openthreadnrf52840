var message_8h =
[
    [ "otBufferInfo", "d1/dec/group__api-message.html#gaadcdabf89dd602ab60efa78fad4f011f", null ],
    [ "otMessage", "d1/dec/group__api-message.html#ga62bdbaf1d492b0c5db59bd8758e38f24", null ],
    [ "otMessagePriority", "d1/dec/group__api-message.html#gaeea6ac34b255ce40ede10862e7178a70", null ],
    [ "otMessageSettings", "d1/dec/group__api-message.html#gaf37e48e3a23d9c9349abd2676a343e42", null ],
    [ "otMessagePriority", "d1/dec/group__api-message.html#gafb260c302eeeeb1a29dce59f7c86d18b", [
      [ "OT_MESSAGE_PRIORITY_LOW", "d1/dec/group__api-message.html#ggafb260c302eeeeb1a29dce59f7c86d18ba3b16d79c57f28c2f525d790897c26631", null ],
      [ "OT_MESSAGE_PRIORITY_NORMAL", "d1/dec/group__api-message.html#ggafb260c302eeeeb1a29dce59f7c86d18ba488f5dc3b298571ef13f603ebfbca25e", null ],
      [ "OT_MESSAGE_PRIORITY_HIGH", "d1/dec/group__api-message.html#ggafb260c302eeeeb1a29dce59f7c86d18ba64a2c55a23103e3249a3d049156eaf31", null ]
    ] ],
    [ "otMessageAppend", "d1/dec/group__api-message.html#gab1cef88f8ddbe6b3e274aaf6bad46c9b", null ],
    [ "otMessageFree", "d1/dec/group__api-message.html#ga70291a9d11f2f2a0bff290bf8ff4ea20", null ],
    [ "otMessageGetBufferInfo", "d1/dec/group__api-message.html#ga5eb9165303d0e72fd9cf9167784c5ab0", null ],
    [ "otMessageGetLength", "d1/dec/group__api-message.html#ga3ae928dc8b31e8808ba1b07abf3dd34b", null ],
    [ "otMessageGetOffset", "d1/dec/group__api-message.html#gafe5a5df3035db36438525eca45d9756d", null ],
    [ "otMessageGetRss", "d1/dec/group__api-message.html#ga0a3269120475ee876e2e655219dea16f", null ],
    [ "otMessageIsLinkSecurityEnabled", "d1/dec/group__api-message.html#ga6d253c0e4d97684e7e31a46abb6c25bc", null ],
    [ "otMessageQueueDequeue", "d1/dec/group__api-message.html#ga37e22ed2f43145d9d537653d5fa3cc2a", null ],
    [ "otMessageQueueEnqueue", "d1/dec/group__api-message.html#ga4d6fe74a30a119aa4ec8f484ed5cccde", null ],
    [ "otMessageQueueEnqueueAtHead", "d1/dec/group__api-message.html#gad367b244988290206e51ab3dd58628af", null ],
    [ "otMessageQueueGetHead", "d1/dec/group__api-message.html#ga71b38c9c904ecead877e9f344e213513", null ],
    [ "otMessageQueueGetNext", "d1/dec/group__api-message.html#gaefee0f07ee9fa021081afb52f74a0776", null ],
    [ "otMessageQueueInit", "d1/dec/group__api-message.html#gab51851baffa16c7f0ba802c8badd129e", null ],
    [ "otMessageRead", "d1/dec/group__api-message.html#ga7d9ded712b0d2bd70e020849a785f1d0", null ],
    [ "otMessageSetDirectTransmission", "d1/dec/group__api-message.html#ga73c39b38aaf0cff5f0a9687576af0ede", null ],
    [ "otMessageSetLength", "d1/dec/group__api-message.html#ga9aeff1951aa15aef4d5ded02ee85bb6b", null ],
    [ "otMessageSetOffset", "d1/dec/group__api-message.html#ga40a0748864a7f7efd9ef7afee724eef6", null ],
    [ "otMessageWrite", "d1/dec/group__api-message.html#gae4d611947a3d3831b5a7a803fa9bc1ef", null ]
];