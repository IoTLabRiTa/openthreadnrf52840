var NAVTREE =
[
  [ "OpenThread", "index.html", [
    [ "Modules", "modules.html", "modules" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"d1/d69/group__radio-config.html#ga0a905ad5c76c5931ee2ef395342f0dca",
"d3/de8/structotBufferInfo.html#a933f8de4b9276e3d9c2898a30b595aaf",
"d5/d3b/group__api-error.html#ggabaaa90aaa35da8f9d394a227a23a6a49ae4248d3f4c69b98ef1c15d46467b76d1",
"d6/d45/group__api-coap.html#ga6ba989a775c21f80a8a98a5bd465af00",
"d7/d43/group__api-channel-manager.html#ga245c48a91b6d9e183fa5e42ccfdf2b0a",
"d8/d18/group__api-jam-detection.html#ga9b7ab151d2a7312bc132f4a2cd78e1c7",
"d9/df7/structotNeighborInfo.html#ab5e8a56565763a90141bdd228bedb2d9",
"da/d24/group__api-thread-general.html#gga6085e64b90c7645b8d206edad0b14e1ca6403ae4fe44464096ac301e6d8363523",
"da/df7/group__plat-ble.html#gaec641541a65313c41e75c0d796b158f0",
"db/dad/group__api-network-time.html#gad9ed788ac8feca011fcb5f5ba4e3576e"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';