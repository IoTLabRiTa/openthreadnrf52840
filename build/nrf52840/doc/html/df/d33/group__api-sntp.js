var group__api_sntp =
[
    [ "otSntpQuery", "d2/d13/structotSntpQuery.html", [
      [ "mMessageInfo", "d2/d13/structotSntpQuery.html#a684342596e193c6380131fe0143904af", null ]
    ] ],
    [ "OT_SNTP_DEFAULT_SERVER_IP", "df/d33/group__api-sntp.html#ga7b68dc817871f8f73d3654e47deaf487", null ],
    [ "OT_SNTP_DEFAULT_SERVER_PORT", "df/d33/group__api-sntp.html#gaf9b46234541eabaa5846d5327e70d910", null ],
    [ "otSntpQuery", "df/d33/group__api-sntp.html#gad8b03f5d57ff303014063b5b74bd1388", null ],
    [ "otSntpResponseHandler", "df/d33/group__api-sntp.html#ga2929103c5f004e8c2b37a8b75932f29a", null ],
    [ "otSntpClientQuery", "df/d33/group__api-sntp.html#gaf59b2e17e5cfaa943f6550fed45720cf", null ],
    [ "otSntpClientSetUnixEra", "df/d33/group__api-sntp.html#ga60816054c21e1fb3b6fb315a9dca435d", null ]
];