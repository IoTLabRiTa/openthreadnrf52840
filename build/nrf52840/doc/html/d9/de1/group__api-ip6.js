var group__api_ip6 =
[
    [ "otIp6Address", "d6/d55/structotIp6Address.html", [
      [ "OT_TOOL_PACKED_FIELD", "d1/dac/unionotIp6Address_1_1OT__TOOL__PACKED__FIELD.html", [
        [ "m16", "d1/dac/unionotIp6Address_1_1OT__TOOL__PACKED__FIELD.html#a4416ac3fc3b67802b7b3168790c15193", null ],
        [ "m32", "d1/dac/unionotIp6Address_1_1OT__TOOL__PACKED__FIELD.html#aef9bb1f19a6f64f189ffe93c12af2185", null ],
        [ "m8", "d1/dac/unionotIp6Address_1_1OT__TOOL__PACKED__FIELD.html#a4316a7ba291880f5aa0e2dbdc8b145c3", null ]
      ] ],
      [ "mFields", "d6/d55/structotIp6Address.html#a8d8766726f527597618b001c5351ab1c", null ]
    ] ],
    [ "otIp6Prefix", "d2/ddd/structotIp6Prefix.html", [
      [ "mLength", "d2/ddd/structotIp6Prefix.html#a4d4c2fecf041ebc7213e474381c49ebf", null ],
      [ "mPrefix", "d2/ddd/structotIp6Prefix.html#ad1f373d25097034f9d36631a1cffd30d", null ]
    ] ],
    [ "otNetifAddress", "d8/d1d/structotNetifAddress.html", [
      [ "mAddress", "d8/d1d/structotNetifAddress.html#af7ae93284e0d8cf84f7fe626107f331c", null ],
      [ "mNext", "d8/d1d/structotNetifAddress.html#a2114aeb81861bfbde4655e93c97b9374", null ],
      [ "mPreferred", "d8/d1d/structotNetifAddress.html#a234f37c483869328a1491f770766b936", null ],
      [ "mPrefixLength", "d8/d1d/structotNetifAddress.html#a34ba4bb9f7701678558399d5bd2ef69f", null ],
      [ "mRloc", "d8/d1d/structotNetifAddress.html#add147629560114319d37f1bc3d0a2298", null ],
      [ "mScopeOverride", "d8/d1d/structotNetifAddress.html#a022f58e53e7d1da6a031d169bf37f58f", null ],
      [ "mScopeOverrideValid", "d8/d1d/structotNetifAddress.html#a9358b4d0c9b1fa7da31f6246ef0d26b6", null ],
      [ "mValid", "d8/d1d/structotNetifAddress.html#a0b2f2997801fa373bc58274b219ee578", null ]
    ] ],
    [ "otNetifMulticastAddress", "de/dc3/structotNetifMulticastAddress.html", [
      [ "mAddress", "de/dc3/structotNetifMulticastAddress.html#aa24fb07dbf3b92fb7941e463f3fe4cef", null ],
      [ "mNext", "de/dc3/structotNetifMulticastAddress.html#ac79817535440549184bb80e62eb15799", null ]
    ] ],
    [ "otSemanticallyOpaqueIidGeneratorData", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html", [
      [ "mDadCounter", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html#a062a58fa1b03bc12098b50d55922b162", null ],
      [ "mInterfaceId", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html#acbedd4f9dc2929f08fc5d5aad7393edc", null ],
      [ "mInterfaceIdLength", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html#af4f10a9d80022bb163beaee70d69a070", null ],
      [ "mNetworkId", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html#aa4c42a9374f5ecf1fb2a9554b1f3cff3", null ],
      [ "mNetworkIdLength", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html#a569cec4cdb384c7cc34687c76b335750", null ],
      [ "mSecretKey", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html#a6619730c24d3392c256b684131362495", null ],
      [ "mSecretKeyLength", "d5/d97/structotSemanticallyOpaqueIidGeneratorData.html#a7b1f96ac0a9050edcbd5d377e632f798", null ]
    ] ],
    [ "otSockAddr", "d3/d28/structotSockAddr.html", [
      [ "mAddress", "d3/d28/structotSockAddr.html#a10d1d646a9b527001349ca83b782cefc", null ],
      [ "mPort", "d3/d28/structotSockAddr.html#ad9b52e6e0906eb7727373a26412a1715", null ],
      [ "mScopeId", "d3/d28/structotSockAddr.html#a5d270a107da02908c99f17e22ad70e9a", null ]
    ] ],
    [ "otMessageInfo", "dd/d76/structotMessageInfo.html", [
      [ "mHopLimit", "dd/d76/structotMessageInfo.html#a6f2c260a3babcee555f67ac4c5feccc7", null ],
      [ "mInterfaceId", "dd/d76/structotMessageInfo.html#a04648c6a181c3dcb3c079f759b0c4cdc", null ],
      [ "mLinkInfo", "dd/d76/structotMessageInfo.html#a6b3a08bac516510224d957bc26d22992", null ],
      [ "mPeerAddr", "dd/d76/structotMessageInfo.html#a44b97257686b6f5fba5afa13b05cc259", null ],
      [ "mPeerPort", "dd/d76/structotMessageInfo.html#ac8684bbbf2e414822ccf9b3e5751527b", null ],
      [ "mSockAddr", "dd/d76/structotMessageInfo.html#a2fcb46a2a2e321c92c519035d72944e4", null ],
      [ "mSockPort", "dd/d76/structotMessageInfo.html#a2edb568cfed99202b24d66dfe786552c", null ]
    ] ],
    [ "OT_IP6_ADDRESS_SIZE", "d9/de1/group__api-ip6.html#gadeeae0fb64b080081b41ebbbcfe06d6e", null ],
    [ "OT_IP6_IID_SIZE", "d9/de1/group__api-ip6.html#ga92c5732c35eea687e818fc8a60ee77a9", null ],
    [ "OT_IP6_PREFIX_SIZE", "d9/de1/group__api-ip6.html#ga2b1f04705445677af9de0d494f0ffe82", null ],
    [ "otIp6Address", "d9/de1/group__api-ip6.html#ga401a73f54d430dbf89474db74fa7884a", null ],
    [ "otIp6AddressCallback", "d9/de1/group__api-ip6.html#gad291b57c2980e033847b92f19629158a", null ],
    [ "otIp6Prefix", "d9/de1/group__api-ip6.html#gab98d28621a5ef9be9be200ef72f16c39", null ],
    [ "otIp6ReceiveCallback", "d9/de1/group__api-ip6.html#ga804caf3017fd73d67cf93e590716394c", null ],
    [ "otIp6SlaacIidCreate", "d9/de1/group__api-ip6.html#ga775ec3e9ea969928bcf64fda4665fcce", null ],
    [ "otMessageInfo", "d9/de1/group__api-ip6.html#ga626382fa8118ffa6fed7d9dab7439a96", null ],
    [ "otNetifAddress", "d9/de1/group__api-ip6.html#ga152e09663f3c8972a8c217a14fef1d80", null ],
    [ "otNetifInterfaceId", "d9/de1/group__api-ip6.html#gaad5fe385eba788646e4a9161094548b6", null ],
    [ "otNetifMulticastAddress", "d9/de1/group__api-ip6.html#gae7c4895df08c58ba2dd0b9e7bbd480be", null ],
    [ "otSockAddr", "d9/de1/group__api-ip6.html#ga966fff91ccb31448707035a32ae4fb76", null ],
    [ "otNetifInterfaceId", "d9/de1/group__api-ip6.html#ga7cc3801495f48d8c607d65e913becd3a", [
      [ "OT_NETIF_INTERFACE_ID_HOST", "d9/de1/group__api-ip6.html#gga7cc3801495f48d8c607d65e913becd3aa34380cef6665d55612062c7c4339b85e", null ],
      [ "OT_NETIF_INTERFACE_ID_THREAD", "d9/de1/group__api-ip6.html#gga7cc3801495f48d8c607d65e913becd3aaeb46c718a5bcd2867ab42939b81dcf3c", null ]
    ] ],
    [ "otIp6AddressFromString", "d9/de1/group__api-ip6.html#ga912d525e407c629484e41a6e6593bfce", null ],
    [ "otIp6AddUnicastAddress", "d9/de1/group__api-ip6.html#ga5f771fbeea628057ddb874c26de32ca5", null ],
    [ "otIp6AddUnsecurePort", "d9/de1/group__api-ip6.html#ga337396785b502c2d933fe9d8e84630e9", null ],
    [ "otIp6CreateMacIid", "d9/de1/group__api-ip6.html#gab73281721c365992fcbeacd619581ff2", null ],
    [ "otIp6CreateRandomIid", "d9/de1/group__api-ip6.html#ga95800a63c0a54558a106a9fd2c65a0ba", null ],
    [ "otIp6CreateSemanticallyOpaqueIid", "d9/de1/group__api-ip6.html#ga98848c764058ba5b6bc79ffea410ac8c", null ],
    [ "otIp6GetMulticastAddresses", "d9/de1/group__api-ip6.html#ga20125582448c6b3101808f726f469f61", null ],
    [ "otIp6GetUnicastAddresses", "d9/de1/group__api-ip6.html#gaeb12d8a7908e9b8eebf38394cd414dde", null ],
    [ "otIp6GetUnsecurePorts", "d9/de1/group__api-ip6.html#ga27c2c884bee5ebca73748237f62187d7", null ],
    [ "otIp6IsAddressEqual", "d9/de1/group__api-ip6.html#gac9c81d7d0880cc1fa77887d5f1411642", null ],
    [ "otIp6IsAddressUnspecified", "d9/de1/group__api-ip6.html#ga6700e6099203f7eeefc2be70d062e072", null ],
    [ "otIp6IsEnabled", "d9/de1/group__api-ip6.html#ga77dcce97c5d6217ed5ef3b814afea356", null ],
    [ "otIp6IsMulticastPromiscuousEnabled", "d9/de1/group__api-ip6.html#gaeee4e0bdac4a247c26616ae7325548f0", null ],
    [ "otIp6IsReceiveFilterEnabled", "d9/de1/group__api-ip6.html#gae3117a710584f3ea0dc494bb8597009f", null ],
    [ "otIp6NewMessage", "d9/de1/group__api-ip6.html#gae8bbc8125663bbf1c74bc4d83b54b984", null ],
    [ "otIp6NewMessageFromBuffer", "d9/de1/group__api-ip6.html#gabedcc64ba37dce564c2d6b73c692f2c0", null ],
    [ "otIp6PrefixMatch", "d9/de1/group__api-ip6.html#gac077a1f1f5cf24cbf051c5fc754663bb", null ],
    [ "otIp6RemoveAllUnsecurePorts", "d9/de1/group__api-ip6.html#ga4e13970081f28575118184a9836d4c22", null ],
    [ "otIp6RemoveUnicastAddress", "d9/de1/group__api-ip6.html#ga3f85adff6dfaa6ed5b7af58b4311114a", null ],
    [ "otIp6RemoveUnsecurePort", "d9/de1/group__api-ip6.html#gacc5cb6166a6a86ea8cd3101c822089d1", null ],
    [ "otIp6SelectSourceAddress", "d9/de1/group__api-ip6.html#ga5375556e1592ace6ec95c8493b22e6a7", null ],
    [ "otIp6Send", "d9/de1/group__api-ip6.html#ga6dc618bef02089e031ee882cd8bfd63c", null ],
    [ "otIp6SetAddressCallback", "d9/de1/group__api-ip6.html#ga5b4ceaf21fec163ece33517a0c28aed9", null ],
    [ "otIp6SetEnabled", "d9/de1/group__api-ip6.html#ga22d15ec3ced47b6b52ac7128e4d63c0f", null ],
    [ "otIp6SetMulticastPromiscuousEnabled", "d9/de1/group__api-ip6.html#gac74c8aaf7b6e137686109929e311fc4f", null ],
    [ "otIp6SetReceiveCallback", "d9/de1/group__api-ip6.html#ga1d8423905816ad0159c9445ed1d6cc42", null ],
    [ "otIp6SetReceiveFilterEnabled", "d9/de1/group__api-ip6.html#ga51cc60fc85f913a7241fc32a64bcf597", null ],
    [ "otIp6SlaacUpdate", "d9/de1/group__api-ip6.html#ga35ac5df361f40ffb0273a1dc5b9c4688", null ],
    [ "otIp6SubscribeMulticastAddress", "d9/de1/group__api-ip6.html#gaaad3f2b2e8bb16aaba23c811796852a0", null ],
    [ "otIp6UnsubscribeMulticastAddress", "d9/de1/group__api-ip6.html#gaf6552c66cbcd0b88fb8efa139c22b22a", null ]
];