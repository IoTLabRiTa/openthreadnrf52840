var network__time_8h =
[
    [ "OT_TIME_SYNC_INVALID_SEQ", "db/dad/group__api-network-time.html#ga8a4cabbc9b868491863df7e04b37fe99", null ],
    [ "otNetworkTimeStatus", "db/dad/group__api-network-time.html#ga74c05dcc5737a4f51232f97b79c897a7", null ],
    [ "otNetworkTimeSyncCallbackFn", "db/dad/group__api-network-time.html#ga06db2f33631c0cd40031f5e5c7c20d1f", null ],
    [ "otNetworkTimeStatus", "db/dad/group__api-network-time.html#gae2ef9060d564f4d373bbe53a631a5af0", [
      [ "OT_NETWORK_TIME_UNSYNCHRONIZED", "db/dad/group__api-network-time.html#ggae2ef9060d564f4d373bbe53a631a5af0a8f2488812ea33555e046a5e1e6575522", null ],
      [ "OT_NETWORK_TIME_RESYNC_NEEDED", "db/dad/group__api-network-time.html#ggae2ef9060d564f4d373bbe53a631a5af0aa72cbb95b5c69f04f91958fe7be76f46", null ],
      [ "OT_NETWORK_TIME_SYNCHRONIZED", "db/dad/group__api-network-time.html#ggae2ef9060d564f4d373bbe53a631a5af0a3104bf1bed5b35eaa9c820a951c29b8e", null ]
    ] ],
    [ "otNetworkTimeGet", "db/dad/group__api-network-time.html#gad9ed788ac8feca011fcb5f5ba4e3576e", null ],
    [ "otNetworkTimeGetSyncPeriod", "db/dad/group__api-network-time.html#gab7d5b4f32a91513ecde9ddfa39f4113d", null ],
    [ "otNetworkTimeGetXtalThreshold", "db/dad/group__api-network-time.html#ga906d642a8278cc00792856423bdd3935", null ],
    [ "otNetworkTimeSetSyncPeriod", "db/dad/group__api-network-time.html#gab61ef2558729a3ac59ea45ddd4872d18", null ],
    [ "otNetworkTimeSetXtalThreshold", "db/dad/group__api-network-time.html#ga431de6ae4845803a84b1e440d6e40d4c", null ],
    [ "otNetworkTimeSyncSetCallback", "db/dad/group__api-network-time.html#ga40ae37e6466a0d533d46b6cfbbde85bc", null ]
];