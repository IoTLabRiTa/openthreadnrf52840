var group__api_cli =
[
    [ "otCliConsoleOutputCallback", "d7/d9d/group__api-cli.html#ga1777e383f674ffdc508daca106e4e075", null ],
    [ "otCliAppendResult", "d7/d9d/group__api-cli.html#gaca493e431d8013359044b27da076c7d0", null ],
    [ "otCliConsoleInit", "d7/d9d/group__api-cli.html#ga2781870a9b576761b6cdc36f2008f604", null ],
    [ "otCliConsoleInputLine", "d7/d9d/group__api-cli.html#gaad905dd76081f98719ef44cfbab0b9d8", null ],
    [ "otCliOutput", "d7/d9d/group__api-cli.html#ga3bfdb5887225a437187fe9abe4751201", null ],
    [ "otCliOutputBytes", "d7/d9d/group__api-cli.html#ga56c4e7cafc2b7764111f128fa9468de6", null ],
    [ "otCliOutputFormat", "d7/d9d/group__api-cli.html#gaca847cb0fb47510381b4f844a551b706", null ],
    [ "otCliPlatLogv", "d7/d9d/group__api-cli.html#gac0bb479ea1529c98a2fc9b8400ca9998", null ],
    [ "otCliSetUserCommands", "d7/d9d/group__api-cli.html#gadb1d6ebd8305ee4e1ebdad25b37dfa61", null ],
    [ "otCliUartInit", "d7/d9d/group__api-cli.html#ga1df0b456dc39c35e4440b0fd49965b0d", null ]
];