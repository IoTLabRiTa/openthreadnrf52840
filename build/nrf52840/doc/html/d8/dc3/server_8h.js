var server_8h =
[
    [ "OT_SERVER_DATA_MAX_SIZE", "d2/ded/group__api-server.html#ga1fcecd055d6f3328de017a1d13aadbad", null ],
    [ "OT_SERVICE_DATA_MAX_SIZE", "d2/ded/group__api-server.html#gafad636b7480da1153404b0ed6856eab4", null ],
    [ "otServerConfig", "d2/ded/group__api-server.html#gab7eb37840d7b34b7ab13b9ebe55cc3ce", null ],
    [ "otServiceConfig", "d2/ded/group__api-server.html#ga1269ce0cec4da001d1622f29ef422b68", null ],
    [ "otServerAddService", "d2/ded/group__api-server.html#ga5f0e5f23cc7a414eff74aac7ad4d8698", null ],
    [ "otServerGetNetDataLocal", "d2/ded/group__api-server.html#ga285055c0a1dcb0550d5d5a64907f7d54", null ],
    [ "otServerGetNextLeaderService", "d2/ded/group__api-server.html#ga3c70f2e0210c33adce17f6ba4ae84568", null ],
    [ "otServerGetNextService", "d2/ded/group__api-server.html#ga8ddc6a820bb5b0b82ab4eca263b5ce5a", null ],
    [ "otServerRegister", "d2/ded/group__api-server.html#ga38e477702ffceeb3e54ea692e5bbab74", null ],
    [ "otServerRemoveService", "d2/ded/group__api-server.html#gad09c8cca1d1bbbfba498de9c0cf3cf01", null ]
];