var group__radio_types =
[
    [ "otExtAddress", "d3/d8f/structotExtAddress.html", [
      [ "m8", "d3/d8f/structotExtAddress.html#a332329394e183841cadef66b7fd3173d", null ]
    ] ],
    [ "otRadioIeInfo", "d5/d04/structotRadioIeInfo.html", [
      [ "mNetworkTimeOffset", "d5/d04/structotRadioIeInfo.html#acbec235ddb7c9d2b2be316334cf9d13d", null ],
      [ "mTimeIeOffset", "d5/d04/structotRadioIeInfo.html#a7cfd2c5c3b95dcb90be0199201fec881", null ],
      [ "mTimestamp", "d5/d04/structotRadioIeInfo.html#ad9023abef8fef969bf5fc6cc8c337317", null ],
      [ "mTimeSyncSeq", "d5/d04/structotRadioIeInfo.html#ae7a8797b6595b0a08321dd299983975a", null ]
    ] ],
    [ "otRadioFrame", "dc/d04/structotRadioFrame.html", [
      [ "mAesKey", "dc/d04/structotRadioFrame.html#ad2691a91c1897ff33efb15836c9659d8", null ],
      [ "mChannel", "dc/d04/structotRadioFrame.html#aaca1d5593ef0b1b6dd940f6a4251dff9", null ],
      [ "mCsmaCaEnabled", "dc/d04/structotRadioFrame.html#af60d0c886464ef6b01e449a34c6a937d", null ],
      [ "mIeInfo", "dc/d04/structotRadioFrame.html#ad6994d824d77c981e4c492f2aeff4fd4", null ],
      [ "mInfo", "dc/d04/structotRadioFrame.html#a6923a03cbab0e86fe7ea56ce1fbfbbb6", null ],
      [ "mIsARetx", "dc/d04/structotRadioFrame.html#ac1d787f53a2e1fd85082492e0dc7d129", null ],
      [ "mLength", "dc/d04/structotRadioFrame.html#ac0bce2a423dd62721ac93390a151de89", null ],
      [ "mLqi", "dc/d04/structotRadioFrame.html#ae89beb2d91bf3c23e274f48430ebc4db", null ],
      [ "mMaxCsmaBackoffs", "dc/d04/structotRadioFrame.html#ad58843a28f1e1a6690827548ced85ed2", null ],
      [ "mMaxFrameRetries", "dc/d04/structotRadioFrame.html#ac663ec320ab9fe4c076e43e7d4c24c68", null ],
      [ "mMsec", "dc/d04/structotRadioFrame.html#a5afd34bc891abe58e45a8c29f9753784", null ],
      [ "mPsdu", "dc/d04/structotRadioFrame.html#a6fc89e98cc7d4dab04eabdbda239a3de", null ],
      [ "mRssi", "dc/d04/structotRadioFrame.html#a5bb7aa8af3467a0c2d845417e42c33e1", null ],
      [ "mRxInfo", "dc/d04/structotRadioFrame.html#a0a85426ffa07aa2b8c5fc9d5a774c8b2", null ],
      [ "mTxInfo", "dc/d04/structotRadioFrame.html#a2a76e70b87f07cbd6fb88f06c2d2c2e6", null ],
      [ "mUsec", "dc/d04/structotRadioFrame.html#aeec8ecd8d6837f5bcba53f21bc69c9f0", null ]
    ] ],
    [ "OT_EXT_ADDRESS_SIZE", "db/d1b/group__radio-types.html#gad61ad5e225afdc13e71ec0b73c51b408", null ],
    [ "OT_PANID_BROADCAST", "db/d1b/group__radio-types.html#ga83a6fca9b333eadabd6f4b77664df1d6", null ],
    [ "otExtAddress", "db/d1b/group__radio-types.html#ga50bea867580b4024e6a099e6b5b11d15", null ],
    [ "otPanId", "db/d1b/group__radio-types.html#ga2dd7c7e7e4b5a8a15e9af7fb78de1eda", null ],
    [ "otRadioCaps", "db/d1b/group__radio-types.html#ga3d28cb6790acb0e25d5923f10ecd22a2", null ],
    [ "otRadioFrame", "db/d1b/group__radio-types.html#gab06907a61c511a92f3fc2bc1514d726b", null ],
    [ "otRadioIeInfo", "db/d1b/group__radio-types.html#ga7846f9d397cdc17486e3f5116aed571f", null ],
    [ "otRadioState", "db/d1b/group__radio-types.html#gabac8258a680edee8c18b7de660f86880", null ],
    [ "otShortAddress", "db/d1b/group__radio-types.html#ga9199b7f0fe13ebcd0da2e3af71f04c40", [
      [ "OT_RADIO_FRAME_MAX_SIZE", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04a4a71c2b96ef99d0f9f6af8beae85071a", null ],
      [ "OT_RADIO_CHANNEL_PAGE", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04aba0d7de66babcfc4477d84fe479b2b44", null ],
      [ "OT_RADIO_CHANNEL_MIN", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04acf9de35f3255b001fa994d99c7bb2bf2", null ],
      [ "OT_RADIO_CHANNEL_MAX", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04a56a9b73111dcd9a15f5c6c8d54d5fe4d", null ],
      [ "OT_RADIO_SUPPORTED_CHANNELS", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04adf9b083b349ef09d3f28b7a9dda35c40", null ],
      [ "OT_RADIO_SYMBOLS_PER_OCTET", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04a46076b5d12c2ad2c11a532aec66dc0fc", null ],
      [ "OT_RADIO_BIT_RATE", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04a51020aa64a3b03169e135accc85fb7f6", null ],
      [ "OT_RADIO_BITS_PER_OCTET", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04a9ea1c56ebbb853f7c075b356e3b52c00", null ],
      [ "OT_RADIO_LQI_NONE", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04a3216e2bca5ece269a53c505a09def06e", null ],
      [ "OT_RADIO_RSSI_INVALID", "db/d1b/group__radio-types.html#ggabc6126af1d45847bc59afa0aa3216b04a11e3d192986996d0901d30811f53bd12", null ],
      [ "OT_RADIO_CAPS_NONE", "db/d1b/group__radio-types.html#ggadc29c2ff13d900c2f185ee95427fb06ca6d5d517247cb67a374004023245ee374", null ],
      [ "OT_RADIO_CAPS_ACK_TIMEOUT", "db/d1b/group__radio-types.html#ggadc29c2ff13d900c2f185ee95427fb06ca580a32c195f9a6fe524f5be907c5fe09", null ],
      [ "OT_RADIO_CAPS_ENERGY_SCAN", "db/d1b/group__radio-types.html#ggadc29c2ff13d900c2f185ee95427fb06caab09bec5a91c5be306904003abb74734", null ],
      [ "OT_RADIO_CAPS_TRANSMIT_RETRIES", "db/d1b/group__radio-types.html#ggadc29c2ff13d900c2f185ee95427fb06ca97a80e5760039794583709129012a023", null ],
      [ "OT_RADIO_CAPS_CSMA_BACKOFF", "db/d1b/group__radio-types.html#ggadc29c2ff13d900c2f185ee95427fb06caa2daca978ba32e2c8d066d6cbba1bef5", null ]
    ] ],
    [ "otRadioState", "db/d1b/group__radio-types.html#ga3f89fd47d15047dbcc4137f00dad6dda", null ]
];